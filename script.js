function tableCreate() {
    var temp = document.querySelector(".MyTable");
    if(temp != null){
        temp.parentNode.removeChild(temp);
    }
  var body = document.body,
    rows = document.getElementById("Row").value,
    columns = document.getElementById("column").value,
    tbl = document.createElement("table");  
    tbl.classList.add("MyTable");
  tbl.style.margin = "15px 100px";
  tbl.style.border = "1px solid black";
  tbl.style.borderCollapse = "collapse";

  for (var i = 0; i < rows; i++) {
    var tr = tbl.insertRow();
    for (var j = 0; j < columns; j++) {
      var td = tr.insertCell();
      td.style.height = "30px";
      td.style.textAlign = "center";
      td.style.border = "1px solid black";
    }
  }
  body.appendChild(tbl);
}
